.. Cypher System Reference documentation master file, created by
   sphinx-quickstart on Mon Aug  1 21:30:42 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Cypher System Reference
======================================

The `Cypher System`_
is a setting-generic tabletop roleplaying game
designed by `Monte Cook Games`_.
This website is a browsable, searchable, online version
of the official Cypher `System Reference Document`_
intended to help Game Masters, players,
and people interested in the Cypher System learn about it.

.. image:: ./_static/Compatible\ with\ the\ Cypher\ System\ Logo\ color\ small.png
    :width: 200pt
    :height: 200pt
    :align: center

.. _Cypher System: https://cypher-system.com/

.. _Monte Cook Games: https://www.montecookgames.com/

.. _System Reference Document: https://csol.montecookgames.com/

.. toctree::
   :maxdepth: 2

   README
   how-to-play
   creating-your-character
   type
   flavor
   descriptor
   focus
   abilities
   equipment
   rules-of-the-game
   experience-points
   genres
   creatures
   npcs
   cyphers
   running-the-cypher-system
   machine-readable-data
   LICENSE

