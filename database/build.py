import csv
from dataclasses import dataclass
import sqlite3
from pathlib import Path
import sys


@dataclass
class ForeignKeyReference:
    from_fields: list[str]
    to_table: str
    to_fields: list[str]

    @classmethod
    def from_foreign_key_row(cls, row: sqlite3.Row) -> "ForeignKeyReference":
        return cls(
            from_fields=[row["from"]],
            to_table=row["table"],
            to_fields=[row["to"]],
        )

    @classmethod
    def from_foreign_key_list(
        cls,
        rows: list[sqlite3.Row],
    ) -> "ForeignKeyReference":
        if not rows:
            raise ValueError("list must contain at least 1 foreign key")

        res = cls.from_foreign_key_row(rows[0])
        for each in rows[1:]:
            assert each["table"] == res.to_table, "list must be one foreign key"
            res.from_fields.append(each["from"])
            res.to_fields.append(each["to"])

        return res


def find_foreign_key_by_number(
    conn: sqlite3.Connection, table: str, number: int
) -> ForeignKeyReference:
    curs = conn.cursor()
    curs.execute(f"PRAGMA foreign_key_list({table});")

    return ForeignKeyReference.from_foreign_key_list(
        [each for each in curs if each["id"] == number]
    )


def build_database(conn: sqlite3.Connection, data_path: Path) -> list[str]:
    print("Building database from:", data_path, file=sys.stderr)

    res = []

    with conn:
        for schema_file in sorted(data_path.glob("*.sql")):
            res.append(str(schema_file))
            print("Loading", schema_file, file=sys.stderr)
            conn.executescript(schema_file.read_text())

        for table_file in data_path.glob("*.csv"):
            res.append(str(table_file))
            print("Loading", table_file, file=sys.stderr)
            table_name = table_file.stem
            with open(table_file, "r") as handle:
                reader = csv.reader(handle)
                headers = next(reader)
                fields = ",".join(headers)
                value_pattern = ",".join("?" for _ in headers)

                sql = f"""
                    INSERT INTO {table_name}({fields})
                    VALUES ({value_pattern});
                """
                conn.executemany(sql, reader)

        # FIXME: Make a better error for multi-field foreign-key errors.
        curs = conn.cursor()
        curs.execute("PRAGMA foreign_key_check;")
        foreign_key_errors = curs.fetchall()
        if foreign_key_errors:
            for each in foreign_key_errors:
                foreign_key_reference = find_foreign_key_by_number(
                    conn, each["table"], each["fkid"]
                )
                from_fields = ",".join(foreign_key_reference.from_fields)
                to_fields = ",".join(foreign_key_reference.to_fields)
                sql = f"""
                    SELECT {from_fields}
                    FROM {each['table']}
                    WHERE rowid=?;
                    """
                curs.execute(sql, (each["rowid"],))
                bad_value = tuple(curs.fetchone())

                source = f"{each['table']}({from_fields})"
                dest = f"{foreign_key_reference.to_table}({to_fields})"
                print(
                    f"{source} value missing from {dest}: {bad_value!r}",
                    file=sys.stderr,
                )

            raise RuntimeError("Foreign key errors detected")

    return res


def main(args: list[str]) -> int:
    data_path = Path(__file__).parent

    conn = sqlite3.connect(args[1])
    conn.row_factory = sqlite3.Row

    build_database(conn, data_path)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
