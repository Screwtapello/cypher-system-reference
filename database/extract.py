from csv import writer
from dataclasses import dataclass
from pathlib import Path
import re
import sys
from typing import Iterable

from database import extract_util

ROLL_RE = re.compile(
    r"""
    (?P<dice_count>[1-9]\d*)d(?P<dice_size>([2-9]|[1-9]\d+))
    (
        \s*\+\s*
        (?P<bonus>\d+)
    )?$
    |
    (?P<fixed>\d+)$
    """,
    re.VERBOSE,
)

DEPLETION_RE = re.compile(
    r"""
    (1[-–])?(?P<target>[1-9]\d*)
    \s
    in
    \s
    1d (?P<dice_size>([2-9]|[1-9]\d+))
    (?P<notes>\s.*)?
    $
    |
    —
    """,
    re.VERBOSE,
)


@dataclass
class ArtifactTemplate:
    name: str
    level_dice_count: int
    level_dice_size: int
    level_bonus: int
    form: str
    effect: str
    depletion_target: int
    depletion_dice_size: int
    depletion_notes: str
    notes: str


def extract_artifacts(
    source: Path, parent_section_id: str
) -> Iterable[ArtifactTemplate]:
    root = extract_util.parse(source.read_text())
    for section in root.iterfind(
        f".//section[@id='{parent_section_id}']/section"
    ):
        children = iter(section)

        name = ""
        level_dice_count = 0
        level_dice_size = 0
        level_bonus = 0
        form = ""
        effect = ""
        depletion_target = 0
        depletion_dice_size = 0
        depletion_notes = ""
        notes = ""

        header = next(children)
        if header is None:
            raise RuntimeError("Artifact has no heading?")
        assert header.tag == "h3"
        if header.text is None:
            raise RuntimeError("Artifact has heading but no text?")
        name = header.text

        deflist = next(children)
        if deflist is None:
            raise RuntimeError("Artifact has no definitions??")
        assert deflist.tag == "dl"

        term = None
        for each in deflist:
            if each.tag == "dt":
                term = each.text
            elif each.tag == "dd":
                if term == "Level:":
                    raw_level = "".join(each.itertext()).strip()
                    match = ROLL_RE.match(raw_level)
                    if match is None:
                        raise ValueError(f"Invalid level roll: {raw_level!r}")
                    groups = match.groupdict()
                    if groups["dice_count"] is not None:
                        level_dice_count = int(groups["dice_count"])
                        level_dice_size = int(groups["dice_size"])
                    if groups["fixed"] is not None:
                        level_bonus = int(groups["fixed"])
                    elif groups["bonus"] is not None:
                        level_bonus = int(groups["bonus"])

                elif term == "Form:":
                    form = "".join(each.itertext()).strip()

                elif term == "Effect:":
                    effect = extract_util.unparse(iter(each))

                elif term == "Depletion:":
                    raw_depletion = "".join(each.itertext()).strip()
                    match = DEPLETION_RE.match(raw_depletion)
                    if match is None:
                        raise ValueError(
                            f"Invalid depletion roll: {raw_depletion!r}"
                        )
                    groups = match.groupdict()
                    if groups["target"] is not None:
                        depletion_target = int(groups["target"])
                        depletion_dice_size = int(groups["dice_size"])
                    if groups["notes"] is not None:
                        depletion_notes = groups["notes"].strip()

                else:
                    raise RuntimeError(f"Unexpected term: {term}")

            else:
                raise RuntimeError(f"Unexpected tag: {each.tag}")

        notes = extract_util.unparse(children)

        yield ArtifactTemplate(
            name,
            level_dice_count,
            level_dice_size,
            level_bonus,
            form,
            effect,
            depletion_target,
            depletion_dice_size,
            depletion_notes,
            notes,
        )

    return []


def extract_all_artifacts(source: Path, dest: Path) -> None:
    artifact_templates_csv = writer(
        open(dest.joinpath("artifact_templates.csv"), "w")
    )
    artifact_templates_csv.writerow(
        (
            "source_name",
            "name",
            "level_dice_count",
            "level_dice_size",
            "level_bonus",
            "form",
            "effect",
            "depletion_target",
            "depletion_dice_size",
            "depletion_notes",
            "notes",
        )
    )

    for each in extract_artifacts(
        source.joinpath("fantasy.html"), "fantasy-artifacts"
    ):
        artifact_templates_csv.writerow(
            (
                "CSRD Fantasy",
                each.name,
                each.level_dice_count,
                each.level_dice_size,
                each.level_bonus,
                each.form,
                each.effect,
                each.depletion_target,
                each.depletion_dice_size,
                each.depletion_notes,
                each.notes,
            )
        )

    for each in extract_artifacts(
        source.joinpath("science-fiction.html"), "science-fiction-artifacts"
    ):
        artifact_templates_csv.writerow(
            (
                "CSRD Science Fiction",
                each.name,
                each.level_dice_count,
                each.level_dice_size,
                each.level_bonus,
                each.form,
                each.effect,
                each.depletion_target,
                each.depletion_dice_size,
                each.depletion_notes,
                each.notes,
            )
        )

    for each in extract_artifacts(
        source.joinpath("horror.html"), "horror-artifacts"
    ):
        artifact_templates_csv.writerow(
            (
                "CSRD Horror",
                each.name,
                each.level_dice_count,
                each.level_dice_size,
                each.level_bonus,
                each.form,
                each.effect,
                each.depletion_target,
                each.depletion_dice_size,
                each.depletion_notes,
                each.notes,
            )
        )

    for each in extract_artifacts(
        source.joinpath("post-apocalyptic.html"), "post-apocalyptic-artifacts"
    ):
        artifact_templates_csv.writerow(
            (
                "CSRD Post-Apocalyptic",
                each.name,
                each.level_dice_count,
                each.level_dice_size,
                each.level_bonus,
                each.form,
                each.effect,
                each.depletion_target,
                each.depletion_dice_size,
                each.depletion_notes,
                each.notes,
            )
        )


def main(argv: list[str]) -> int:
    dest_path = Path(__file__).parent
    source_path = Path(argv[1])

    extract_all_artifacts(source_path, dest_path)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
