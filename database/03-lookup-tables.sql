-- Defines the basic categories of armor.
CREATE TABLE armor_categories
    -- The source where this category was defined
    ( source_name TEXT NOT NULL
        REFERENCES sources(source_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    -- The name of this category
    , name TEXT NOT NULL
    -- The additional cost for levels of speed effort
    -- while wearing armor of this category,
    -- as described in the "Using Armor" section
    -- of the Equipment chapter.
    , default_speed_effort_cost INTEGER NOT NULL
    -- The amount subtracted from incoming damage
    -- while wearing armor of this category,
    -- as described in the "Armor" section
    -- of the Rules of the Game chapter.
    , default_rating INTEGER NOT NULL
        CHECK (default_rating > 0)
    , PRIMARY KEY (source_name, name)
    ) STRICT;


-- Defines the basic categories for item prices.
--
-- See "Price Categories" in the Equipment chapter.
CREATE TABLE price_categories
    -- The source where this category was defined
    ( source_name TEXT NOT NULL
        REFERENCES sources(source_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    -- The name of this category
    , name TEXT NOT NULL
    -- An order-of-magnitude for this category.
    -- Mostly here to allow price categories
    -- to be sorted.
    , scale INTEGER NOT NULL
    , PRIMARY KEY (source_name, name)
    ) STRICT;

-- Defines the basic categories for ranges.
--
-- A range can be any fixed distance,
-- but this table provides convenient names
-- for the most common ranges.
--
-- See "Range and Speed" in the How to Play the
-- Cypher System chapter.
CREATE TABLE range_categories
    -- The source where this category was defined
    ( source_name TEXT NOT NULL
        REFERENCES sources(source_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    -- The maximum distance in feet of this range.
    , range INTEGER NOT NULL CHECK (range >= 0)
    -- A human-readable name for this range.
    , name TEXT NOT NULL
    , PRIMARY KEY (source_name, range)
    ) STRICT;

-- Defines the basic categories for weapons.
--
-- See "Weapons" in the Equipment chapter.
CREATE TABLE weapon_categories
    -- The source where this category was defined
    ( source_name TEXT NOT NULL
        REFERENCES sources(source_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    -- The name of this category
    , name TEXT NOT NULL
    -- The amount of damage this kind of weapon deals
    , default_damage INTEGER NOT NULL
        CHECK (default_damage > 0)
    -- A difficulty modifier for
    -- attacks made with this weapon.
    , default_attack_difficulty_modifier INTEGER NOT NULL
        DEFAULT 0
    , PRIMARY KEY (source_name, name)
    ) STRICT;

-- Defines the resource pools a player character has
--
-- See "Character Stats" in the Creating Your Character
-- chapter.
CREATE TABLE pools
    -- The source where this pool was defined
    ( source_name TEXT NOT NULL
        REFERENCES sources(source_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    -- The name of this pool
    , name TEXT NOT NULL
    -- A number that determines what order this pool appears in a list
    , sort_key INTEGER NOT NULL
    , PRIMARY KEY (source_name, name)
    ) STRICT;


-- Defines the levels of training a player character can have
--
-- See "Skills" in the Creating Your Character chapter.
CREATE TABLE skill_training
    -- The source where this training level was defined
    ( source_name TEXT NOT NULL
        REFERENCES sources(source_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    -- The name of this training level
    , name TEXT NOT NULL
    -- The difficulty modifier this level of training applies.
    , difficulty_modifier INTEGER NOT NULL
    , PRIMARY KEY (source_name, name)
    ) STRICT;
