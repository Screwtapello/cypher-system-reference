-- Defines sources of Cypher System data.
CREATE TABLE sources
    -- A human-readable name for a data-source,
    -- such as "Cypher System Reference Document",
    -- "Numenera: Discovery",
    -- or "The Stars Are Fire".
    ( source_name TEXT NOT NULL PRIMARY KEY
    -- A human-readable description of this source.
    -- It might mention the author, publisher,
    -- copyright information, a website, a place
    -- where the full product can be purchased,
    -- that kind of thing.
    , description TEXT NOT NULL
    ) STRICT;

-- Defines which sources build on other sources.
--
-- For example, the chapter on the Superheroes genre
-- suggests that games in the Superheroes genre will
-- likely include the same kinds of equipment and NPCs
-- as a game in the Modern genre, so the "CSRD
-- Superheroes" source should depend on the "CSRD
-- Modern" source.
CREATE TABLE source_dependencies
    ( from_source TEXT NOT NULL
        REFERENCES sources(source_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    , to_source TEXT NOT NULL
        REFERENCES sources(source_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    , UNIQUE (from_source, to_source)
    )
    STRICT;

-- Calculates all the sources a given sources depends on,
-- directly or indirectly.
--
-- For example, if the "CSRD Superheroes" source
-- depensd on the "CSRD Modern" sourcee,
-- which depends on the "CSRD Core" source,
-- then a query like:
--
--   SELECT to_source
--   FROM source_indirect_dependencies
--   WHERE from_source = 'CSRD Superheroes';
--
-- ...should return "CSRD Core", "CSRD Modern" and
-- "CSRD Superheroes".
CREATE VIEW source_indirect_dependencies AS
WITH RECURSIVE
    dependencies(from_source, to_source) AS
    ( SELECT from_source, to_source
        FROM source_dependencies
        UNION
        SELECT l.from_source, r.to_source
        FROM dependencies as l
             INNER JOIN source_dependencies as r
             ON l.to_source = r.from_source
    )
SELECT from_source, to_source
FROM dependencies;
