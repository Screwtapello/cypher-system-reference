CREATE TABLE artifact_templates
    ( source_name TEXT NOT NULL
        REFERENCES sources(source_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    -- The name of this template
    , name TEXT NOT NULL
    -- The number of dice to roll when picking a level.
    -- The X in "XdY + Z", it must be a positive number.
    , level_dice_count INTEGER NOT NULL
        CHECK (level_dice_count >= 0)
    -- The size of the dice to roll when picking a level.
    -- The Y in "XdY + Z"
    , level_dice_size INTEGER NOT NULL
    -- The bonus to add when picking a level.
    -- The Z in "XdY + Z"
    , level_bonus INTEGER NOT NULL
    -- A physical decription of the artifact.
    , form TEXT NOT NULL DEFAULT ''
    -- The effect of activating this artifact.
    -- This field contains arbitrary HTML.
    , effect TEXT NOT NULL
    -- The number to roll over for depletion checks.
    -- The X in "1-X in 1dY"
    , depletion_target INTEGER NOT NULL
        CHECK (depletion_target >= 0)
    -- The size of the die to roll for checking depletion.
    -- The Y in "1-X in 1dY"
    , depletion_dice_size INTEGER NOT NULL
        CHECK (depletion_dice_size >= 0)
    -- Any extra notes about depletion checks.
    , depletion_notes TEXT NOT NULL DEFAULT ''
    -- Any additional comments about the artifact.
    -- This field contains arbitrary HTML.
    , notes TEXT NOT NULL
    , PRIMARY KEY (source_name, name)
    , CHECK (
        CASE
            -- If we are rolling zero dice...
            WHEN level_dice_count = 0
            -- ...then we don't need a die size.
            THEN level_dice_size = 0
            -- Otherwise, the size must be at least a d2.
            ELSE level_dice_size >= 2
        END
      )
    , CHECK (
        CASE
            -- If the artifact does not deplete...
            WHEN depletion_target = 0
            -- ...then it we don't need a die size
            THEN depletion_dice_size = 0
            ELSE
                -- Otherwise, the size must be at least a d2,
                depletion_dice_size > 2
                -- and the target must be rollable on the die
                AND depletion_target < depletion_dice_size
        END
      )
    ) STRICT;
