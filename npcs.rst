NPCs
####

NPCs in this chapter are generic examples of nonplayer characters
that can be used in many genres.
These NPCs follow the template used in the :doc:`creatures` chapter.

Reskinning NPCs:
    GMs will find that with a few tweaks, a guard can be a
    modern-day cop, a fantasy caravan guard, or a science fiction drone
    soldier. This is known as reskinning—making slight changes to existing
    stats to customize the NPC for your own game.

Health, Not Pools:
    Remember that NPCs don’t have stat Pools. Instead,
    they have a characteristic called health. When an NPC takes damage of
    any kind, the amount is subtracted from its health. Unless described
    otherwise, an NPC’s health is always equal to its target number. Some
    NPCs might have special reactions to or defenses against attacks that
    would normally deal Speed damage or Intellect damage, but unless the
    NPC’s description specifically explains this, assume that all damage is
    subtracted from the NPC’s health.

Appropriate Weapons:
    NPCs use weapons appropriate to their situation,
    which might be swords and crossbows, knives and shotguns, malefic
    psychic weapons, blasters and grenades, and so on.

Basic NPCs
==========

Many NPCs are simple and understandable enough to be encapsulated just
by their level and a few other other relevant stats.

Cannibal:
    level 3, deception and other interaction tasks as level 6;
    health 12

Hacker:
    level 2; programming, digital infiltration, and repairing
    computers as level 7

Mad scientist:
    level 4, most actions as level 6 due to gadgets, serums,
    artifacts, etc.

Marauder:
    level 4, initiative and intimidation as level 7; health 28;
    Armor 1

Master detective:
    level 5; perception, intuition, initiative, and
    detecting falsehood as level 9

Politician:
    level 2, all interaction tasks as level 6

Priest:
    level 2, religious lore and all interaction tasks as level 6

Professor:
    level 2, knowledge of science and all interaction tasks as
    level 6

Soldier:
    level 3, perception as level 4; health 12; Armor 1; attacks
    inflict 5 points of damage

List of NPCs
============

This chapter includes details about the following NPCs:
:ref:`npcs:Assassin 6 (18)`,
:ref:`npcs:Crime Boss 3 (9)`,
:ref:`npcs:Detective 3 (9)`,
:ref:`npcs:Guard 2 (6)`,
:ref:`npcs:Occultist 5 (15)`,
:ref:`npcs:Secret Agent 5 (15)`,
:ref:`npcs:Thug 3 (9)`,
:ref:`npcs:Wizard, Mighty 8 (24)`.

The :doc:`genres` chapter also includes suggestions
for NPCs appropriate to various genres.

Assassin 6 (18)
---------------

An assassin kills with poison, with high-velocity bullets from a
distance, or by arranging for an unfortunate accident. Assassins accept
contracts from governments, corporations, crime bosses, and aggrieved
former partners, though some assassins pay themselves by tracking
criminals anywhere to collect on “dead or alive” bounties.

Motive:
    Murder (usually for hire)

Health:
    18

Damage Inflicted:
    6 points

Armor:
    1

Movement:
    Short

Modifications:
    Stealth and deception tasks as level 8; when attacking
    from hiding, melee and ranged attacks as level 7

Combat:
    An assortment of small weapons are hidden about an assassin’s
    body. They can also coat their weapons or ammo with a level 6 poison
    that moves victims who fail a Might defense roll one step down the
    damage track.

Interaction:
    Some assassins have a sort of integrity about their work
    and can’t be dissuaded from completing their contracts with bribes.

Use:
    An assassin is greatly feared by anyone with powerful, wealthy
    enemies.

Loot:
    Aside from their weapons and poisons, most assassins have currency
    equivalent to a very expensive item and maybe one or two cyphers.

GM intrusion:
    The character loses their next turn, stunned, after
    recognizing the assassin to be the same murderer who killed someone
    important to them in the past.

Crime Boss 3 (9)
----------------

A crime boss usually isn’t physically powerful but wields power through
lies, bribery, and control. Rarely encountered alone, they rely on
guards, thugs, and other measures to provide physical security. A crime
boss could be a petty noble, a mafia king, or the captain of a pirate
ship that sails the seas or glides the space lanes.

Motive:
    Money and power

Health:
    12

Damage Inflicted:
    5 points

Armor:
    1

Movement:
    Short

Modifications:
    Deception, persuasion, intimidation, and tasks related to
    friendly interaction as level 7

Combat:
    Guards, thugs, and other followers deal 1 additional point of
    damage when the crime boss can see them and issue commands. If possible,
    crime bosses fight while mounted or in a vehicle, directing their
    followers from the rear of any conflict, concentrating first on issuing
    orders.

Interaction:
    Crime bosses are committed to their plans, whatever those
    might be. Most bosses rely on a lieutenant or trusted thug to interact
    with people in their place.

Use:
    A crime boss and their followers execute a heist on a secure
    location and take hostages when things go south. Someone must go in and
    talk to the crime boss to defuse the situation.

Loot:
    A crime boss has currency equivalent to a very expensive item in
    addition to weapons, medium armor, and miscellaneous gear.

GM intrusion:
    The crime boss uses a clever trick or cypher to block all
    incoming attacks in a given round of combat.

Detective 3 (9)
---------------

Detectives are usually veterans of their organization (such as the
police, city watch, marshals, space command, and so on) with extensive
experience. Some detectives are freelance sleuths whose uncanny ability
to see the truth comes from personal training combined with an
underlying talent for noticing clues that others miss.

Motive:
    Solve the crime

Health:
    12

Damage Inflicted:
    4 points

Movement:
    Short

Modifications:
    Tasks relating to perception, intuition, initiative, and
    detecting falsehoods as level 6

Combat:
    Detectives prefer to outwit their foes rather than engage in a
    straight-up fight. Even then, most conflicts occur in a place and time
    of the detective’s choosing, preferably in the presence of their allies.
    A detective can deduce weaknesses of their enemies (if any) and exploit
    them in combat.

Interaction:
    Some detectives are insufferable
    know-it-alls. Others have learned that humility is also a useful tool
    for getting answers from people.

Use:
    To the PCs, detectives can be obstacles (a detective is on their
    trail), allies (a detective helps them assemble clues), or both, but the
    sleuths are rarely a way for the characters to hand off responsibility
    for accomplishing a hard task.

Loot:
    Aside from their weapons, most detectives have currency equivalent
    to a very expensive item and a cypher.

GM intrusion:
    The detective intuits the character’s next attack and
    moves perfectly so that an ally of the character takes the attack
    instead.

Guard 2 (6)
-----------

Guards keep the peace but don’t usually show much initiative.
Ultimately, they do as they’re ordered by their superiors, regardless of
legality. A guard might be a star trooper dressed in intimidating armor,
a mall security guard, a beat police officer, or a mafia goon.

(When attacked, guards always call for the help of other guards, if
possible.)

Motive:
    Keep the peace; follow orders

Health:
    8

Damage Inflicted:
    3 points

Armor:
    1 or 2

Movement:
    Short

Modifications:
    Perception as level 3

Combat:
    Guards are not often wily, but they understand strength in
    numbers. If two or more guards attack the same target with at least one
    melee attack in the same round, the target’s Speed defense roll against
    those attacks is hindered.

Interaction:
    Interacting with a guard typically involves one issue:
    does
    the PC want to do something that the guard has been told to prevent? If
    so, the PC could have a difficult time.

Use:
    To the PCs, guards can be allies, obstacles, or both. Guards who
    serve the public good have their own duties and aren’t interested in
    doing the characters’ work for them.

Loot:
    A guard has currency equivalent to an inexpensive item in addition
    to weapons, armor, and basic gear.

GM intrusion:
    1d6 local citizens intervene on the guard’s behalf,
    calling for more guards or even fighting the guard’s foes.

Occultist 5 (15)
----------------

Paranormal researchers, cultists, secret practitioners of white magic,
and coven members might be occultists. Thanks to their study of the
metaphysical, occultists learn several magical tricks, including the
ability to summon or banish the dead.

Health:
    15

Damage Inflicted:
    5 points

Movement:
    Short

Modifications:
    Knowledge of occult topics and rituals as level 8;
    ability to detect lies and tricks as level 2

Combat:
    An occultist has a charm or device for summoning a level 5
    spirit or demon that will do their bidding for ten minutes. Some also
    have (or instead have) a spell, item, or device that inflicts 5 points
    of damage on normal creatures within long range, and 10 points of damage
    on a demon or spirit (or, instead of dealing extra damage, the effect
    confines the demon or spirit in some way).

Interaction:
    Occultists are deeply concerned with spiritual or demonic
    matters and see those influences in all things, whether those influences
    exist or not. That makes them amenable to persuasion and deception, if
    couched in the language of spiritual influence.

Use:
    To find a needed answer, the spirit of a dead person must be
    questioned. Alternatively, a haunting presence must be banished. Either
    way, the task requires an occultist.

Loot:
    In addition to their clothing and mundane weapons, occultists have
    currency equivalent to an inexpensive item, a cypher, and possibly an
    artifact related to their power over spirits or demons.

GM intrusion:
    A bony hand erupts from the ground at the character’s
    feet. On a failed Speed defense roll, they are held in place until they
    succeed on a Might-based task to escape. Each round the character fails
    to escape, the hand squeezes for 3 points of damage.

Secret Agent 5 (15)
-------------------

Secret agents are trained professionals who put their mission before
their own well-being, regardless of which government agency,
corporation, guild, or kingdom employs them. An agent operates under a
fake cover, perhaps as an envoy, inspector, technician, actor, tourist,
or bumbling fool.

Motive:
    Accomplish the goals of the employer while maintaining cover

Health:
    15

Damage Inflicted:
    5 points

Movement:
    Short

Modifications:
    Tasks related to disguise and deceiving as level 6

Combat:
    A secret agent always has a covert, unexpected backup weapon
    that they can use to make a surprise attack, such as a ring or glove
    with a hidden poisoned needle (dealing 5 points of Speed damage that
    ignore Armor), a fake tooth filled with poison gas to blow in a victim’s
    face (inducing sleep for ten minutes), or a ring with a miniature gun.

Interaction:
    Secret agents are confident, masterful, and always give the
    impression of being one step ahead of the game, even when caught off
    guard.

Use:
    As an ally, a secret agent can guide the PCs to their next mission,
    fill in gaps in their knowledge, and warn them of dangers. If the
    characters encounter an unfriendly agent, the NPC likely pretends to be
    a friend.

Loot:
    Agents typically have currency equivalent to an expensive item, a
    couple of cyphers, tools for spying and maintaining their cover, and
    possibly an artifact.

GM intrusion:
    The secret agent produces a cypher that, for the rest
    of the day, eases all tasks by two steps.

Thug 3 (9)
----------

Thugs are usually rough, crude, and harsh individuals who prey on those
who follow the rules. A thug might be a streetwise drug dealer, a bandit
who hunts lone travelers in the wilds, a savage warrior adroit with
ranged weapons, or a cyberbully among pacifists. Most thugs work for
themselves, but they may employ gangs of guards to help them conduct
their business.

Motive:
    Take what they want

Health:
    9

Damage Inflicted:
    4 points

Armor:
    1

Movement:
    Short

Combat:
    Thugs prefer ambushes, making ranged attacks from hiding if
    possible. Sometimes they spoil the ambush to issue an ultimatum before
    attacking: give us your valuables or you’ll be sorry.

Interaction:
    Thugs are interested in money and power, which means they
    almost always accept bribes. If faced with a real threat, thugs usually
    retreat.

Use:
    Thugs are everywhere, sometimes accompanied by guards who are
    equally malicious but not quite as powerful.

Loot:
    A thug has currency equivalent to an inexpensive item in addition
    to weapons, shields, and light armor. One thug in a group might have a
    cypher.

GM intrusion:
    Another thug, hidden until just the right moment, appears
    and takes a shot with a ranged weapon before joining the fray.

Wizard, Mighty 8 (24)
---------------------

Some wizards learn so many spells and accumulate so much lore that they
become incredibly powerful. Some work for a higher purpose, whereas
others are concerned only with themselves.

Motive:
    Seek powerful sources of magic (to collect or to keep safe)

Health:
    40

Damage Inflicted:
    8 points

Movement:
    Short

Modifications:
    All tasks related to knowledge of arcane lore as level 9

Combat:
    When a wizard makes a long-range attack with their staff or
    strikes someone with it, arcane energy damages the target and, if
    desired, all creatures the wizard selects within short range of the
    target. Targets that are within immediate range of the wizard when they
    take damage are thrown out of immediate range.

    A mighty wizard knows many spells, including spells that grant +5 to
    Armor for an hour, spells of teleportation, spells of finding, and so
    on. A wizard also likely carries several cyphers useful in combat.

Interaction:
    Care should be taken when negotiating with wizards because
    they are subtle and quick to anger. Even when negotiations succeed, a
    wizard’s suggestions are usually cryptic and open to interpretation. A
    mighty wizard might be convinced to teach a character how to cast a
    spell.

Use:
    A wizard is putting together a team to challenge a great foe, and
    the PCs fit the bill.

Loot:
    A mighty wizard has 1d6 cyphers.

GM intrusion:
    The wizard casts two spells as a single action instead of
    just one.
