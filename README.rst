About This Website
##################

This website is a browsable, searchable, online version
of the official Cypher `System Reference Document`_
intended to help Game Masters, players,
and people interested in the `Cypher System`_ learn about it.

.. _System Reference Document: https://csol.montecookgames.com/

.. _Cypher System: https://cypher-system.com/


Goals
=====

The intent is to make a copy of the Cypher SRD that is:

* readable for humans, much like the original Cypher System Revised book
* accessible through search, categorisation, and hyperlinks
* machine-parseable, to aid in the development of automated play aids
* annotated, with notes and explanations
  about parts of the text that are confusing,
  even without official errata

Non-Goals
=========

There's lots of things this project *could* be but won't be:

* profitable, though advertising or licensing

  * we're not in this to make money, just to make Cypher more popular

* a homebrew repository

  * content (types, foci, descriptors, cyphers, creatures, etc.)
    not in the official SRD should not be listed here
  * although I hope somebody does make that

* a recreation of the Cypher System Revised book

  * just because some text was a heading or a sidebar
    or a margin note in the CSR
    doesn't mean it needs to be presented exactly that way here

Reporting Problems and Suggestions
==================================

If you find a problem with any of the content on this website
(such as a misspelled word, or missing content)
please first check the official `System Reference Document`_
to see if the problem is present there.
If you do find a problem in the official SRD,
please `report it directly`_ to Monte Cook Games:

.. _report it directly: https://support.montecookgames.com/contact/csol-support/

If the problem is not in the SRD,
or if it is a problem with formatting
(such as heading text that looks like body text,
or broken links)
please report it to `our issue tracker`_.

.. _our issue tracker: https://gitlab.com/Screwtapello/cypher-system-reference/-/issues

Contributing Improvements
=========================

If you would like to contribute improvements to the site
in line with the `goals`_ and `non-goals`_ above
(such as adding links,
fixing capitalisation,
or making formatting more consistent or more machine-readable)
please be aware that contributions must be provided under
the :doc:`CC0-1.0` licence,
so that others can improve and build on them in turn.

The complete content of this website is available
as a Git repository:

    https://gitlab.com/Screwtapello/cypher-system-reference/

Please feel free to submit pull requests/merge requests.
If you're not comfortable using Git,
you can file an issue in `our issue tracker`_ instead.

Testing improvements
--------------------

If you would like to you build your own copy of the site
to test out your changes before submitting them,
you will need to install the `Sphinx`_ documentation system.

.. _Sphinx: https://www.sphinx-doc.org/en/master/usage/installation.html

You may optionally also install the `GraphViz`_ diagram tool.
If you don't,
then the :ref:`schema diagram on the machine readable data page
<machine-readable-data:schema diagram>`
will not work,
but the rest of the website should work properly.

.. _GraphViz: https://www.graphviz.org/download/

Once you have these tools installed,
run the command ``make html``
in the directory containing all the ``.rst`` files.
The website should be produced in the ``_build/html`` subdirectory.

Conventions
===========

When marking up the SRD text,
we try to follow these conventions.

Notes and annotations
---------------------

There's various parts of the SRD text
known to be confusing or misleading,
or likely errors introduced as the text went
from *Numenera* to *Cypher System* to *Cypher System Revised*.
Although we won't slavishly follow the structure and formatting
of the original text,
we won't wholesale rewrite it either.
Instead, we'll put a note immediately after it, like this:

.. note::

   This is an example note.

Heading order
-------------

We use the following header-underline characters
to build the document hierarchy:

* ``#`` Titles
* ``=`` Chapters
* ``-`` Sections
* ``~`` Sub-sections
* ``'`` Sub-subsections

Definition lists
----------------

There are many parts of the SRD intended to be rendered with a "mini heading"
in bold at the beginning of each paragraph. I have chosen to mark these up as
definition lists. Even though that's not the default presentation of definition
lists in most browsers, it can be achieved through CSS and it makes the content
easier for computers to extract.

Tables
------

Tables should have a proper header row,
not just an ordinary row with bold text.

We should probably use one table syntax for all the tables in the SRD.
Pandoc's conversion seems to have picked "simple table" syntax where it could,
and "grid table" syntax for the others,
but that means similar tables wind up using different formats
because one needed to word-wrap inside a cell.

Hyperlinks
----------

The official SRD document has no hyperlinks,
but we should add them wherever they'd be useful.

If you want to link to the "Foo Bar" header
in ``some-document.rst``,
you can use the following syntax::

    :ref:`some-document:Foo Bar`

You should probably use that syntax
even for linking to a header in the same document,
in case the text is later broken out to a different document.

However, abilities are special.
Because they do not actually have headers (yet),
they do not have automatic links,
so they've been given manual links instead.
Linking to an ability looks like this::

    :ref:`Bash <Bash>`

It's ugly and redundant, but we'll fix it eventually.
