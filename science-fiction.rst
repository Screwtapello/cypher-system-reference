Science Fiction
###############

Science fiction is an incredibly broad category. It covers UFOs, space
opera, near-future dystopias, otherworldly epics, hard science fiction,
and everything in between. Even when compared to fantasy, science
fiction is so wide that it almost isn’t a single genre at all.
Truthfully, there’s not all that much to tie, say, *The Time Machine* by
H. G. Wells with a dark cyberpunk story except for the technology
involved, which is at a higher level than we possess or understand
today. But even that part of science fiction is contentious. Should the
science be purely that which obeys the laws of physics as we understand
them today (often called hard science fiction), or is it more of an
“anything goes” proposition? Is science we can’t explain really just
magic?

For our purposes, we’ll treat fantastic science fiction as the default:
aliens, spaceships that allow travel to other stars, energy weapons and
shields, and so on. It’s a familiar setting to almost everyone
interested in science fiction. That said, we’ve also got some additional
guidance for hard science fiction, where what’s possible is more
grounded in what we currently scientifically extrapolate. But your
science fiction setting can be anything you can imagine.

Suggested Types for a Science Fiction Game
==========================================

============== =========================================
Role           Type
============== =========================================
Soldier        Warrior
Technician     Explorer with technology flavor
Pilot          Explorer with technology flavor
Diplomat       Speaker
Doctor         Speaker with skills and knowledge flavor
Spy            Explorer with stealth flavor
Scientist      Explorer with skills and knowledge flavor
Psion          Adept
Psychic knight Warrior with magic flavor
============== =========================================

Basic Creatures and NPCs for a Science Fiction Game
===================================================

Innocuous rodent:
    level 1

Guard beast:
    level 3, perception as level 4

Corporate drone:
    level 2

Physical laborer:
    level 2; health 8

Additional Science Fiction Equipment
====================================

In a science fiction setting, the following items (and anything else
appropriate to the setting) are usually available.

Inexpensive Items
-----------------

.. table::
   :width: 100%
   :widths: 40 60

   ====================== ============
   Weapons                Notes
   ====================== ============
   Energy pack (50 shots)
   Knife (simple)         Light weapon
   ====================== ============

.. table::
   :width: 100%
   :widths: 40 60

   ======================== =====
   Other Items              Notes
   ======================== =====
   Flashlight
   Survival rations (1 day)
   ======================== =====

Moderately Priced Items
-----------------------

.. table::
   :width: 100%
   :widths: 40 60

   =================== ===============================================
   Weapons              Notes
   =================== ===============================================
   Hunting knife        Light weapon
   Machete              Medium weapon
   Grenade (sonic)      Explosive weapon, inflicts 2 points of damage
                        in immediate radius, plus Might defense roll
                        or lose next turn
   Grenade (thermite)   Explosive weapon, inflicts 5 points of damage
                        in immediate radius
   =================== ===============================================

.. table::
   :width: 100%
   :widths: 40 60

   ============== ===========
   Armor          Notes
   ============== ===========
   Leather jacket Light armor
   ============== ===========

.. table::
   :width: 100%
   :widths: 40 60

   =================== ===================================
   Other Items         Notes
   =================== ===================================
   Backpack
   Bag of heavy tools
   Bag of light tools
   Binoculars          Asset for perception tasks at range
   Breather            8 hours of breathable air
   Climbing gear       Asset for climbing tasks
   Communicator        Planetary range
   Crowbar
   Environment tent
   First aid kit       Asset for healing tasks
   Handcuffs
   Nightvision goggles
   Portable lamp
   Rope                Nylon, 50 feet
   Sleeping bag
   =================== ===================================

Expensive Items
---------------

.. table::
   :width: 100%
   :widths: 40 60

   =============== ===================================================
   Weapons          Notes
   =============== ===================================================
   Light blaster    Light weapon, short range
   Medium blaster   Medium weapon, long range
   Needler          Light weapon, long range
   Shotgun          Heavy weapon, immediate range
   Stunstick        Medium weapon, inflicts no damage but human-sized
                    or smaller target loses next action
   =============== ===================================================

.. table::
   :width: 100%
   :widths: 40 60

   ====================== ============
   Armor                  Notes
   ====================== ============
   Armored bodysuit       Medium armor
   Lightweight body armor Medium armor
   ====================== ============

.. table::
   :width: 100%
   :widths: 40 60

   =================================== ================================
   Other Items                         Notes
   =================================== ================================
   Camera designed to be concealed     Transmits at long range
   Microphone designed to be concealed Transmits at long range
   Environment suit                    Provides 24 hours of atmosphere
                                       and +10 to Armor against extreme
                                       temperatures
   Wrist computer                      Asset for most knowledge-based
                                       tasks
   =================================== ================================

Very Expensive Items
--------------------

.. table::
   :width: 100%
   :widths: 40 60

   =================== ============================================
   Weapons             Notes
   =================== ============================================
   Heavy blaster       Heavy weapon, long range
   Heavy blaster rifle Heavy weapon, 300-foot (90 m) range
   Pulse laser gun     Medium weapon, rapid-fire weapon, long range
   =================== ============================================

.. table::
   :width: 100%
   :widths: 40 60

   ========== ===========================================
   Armor      Notes
   ========== ===========================================
   Battlesuit Heavy armor, also works as environment suit
   ========== ===========================================

.. table::
   :width: 100%
   :widths: 40 60

   ================== ================================================
   Other Items        Notes
   ================== ================================================
   Disguise kit       Asset for disguise tasks
   Gravity regulator  Belt-mounted device that regulates gravity to
                      1G for wearer if within 0 G to 3 G conditions
   Handheld scanner   Asset for identifying tasks
   Hovercraft         Level 4
   Infiltrator        Asset for lockpicking tasks when used with
                      electronic locks
   Jetpack            Level 4
   Stealthsuit        Asset for stealth tasks
   ================== ================================================

Exorbitant Items
----------------

.. table::
   :width: 100%
   :widths: 40 60

   ============= =====================================================
   Weapons        Notes
   ============= =====================================================
   Blast cannon   10 points of damage, 500-foot (150 m) range,
                  requires a tripod and two people to operate
   ============= =====================================================

.. table::
   :width: 100%
   :widths: 40 60

   =========== =============================
   Armor       Notes
   =========== =============================
   Force field Not armor, offers +1 to Armor
   =========== =============================

.. table::
   :width: 100%
   :widths: 40 60

   =============== =======
   Other Items     Notes
   =============== =======
   Luxury hovercar Level 5
   Robot servant   Level 3
   Small spaceship Level 4
   =============== =======

Science Fiction Artifacts
=========================

Artifacts in a science fiction game can be strange relics from an
unknown alien source or tech items that aren’t yet widely available. In
a galactic setting, for example, it’s easy to imagine that innovations
or specialized items might not have spread everywhere.

.. sqlquery::
   SELECT DISTINCT
       name,
       CASE
           WHEN level_dice_count <> 0 AND level_bonus <> 0
               THEN level_dice_count || 'd' || level_dice_size ||
                    ' + ' || level_bonus
           WHEN level_dice_count <> 0 AND level_bonus = 0
               THEN level_dice_count || 'd' || level_dice_size
           WHEN level_dice_count = 0 AND level_bonus <> 0
               THEN level_bonus || ''
       END as level,
       form,
       effect,
       CASE
           WHEN depletion_target == 0
               THEN '—'
           WHEN depletion_target == 1
               THEN '1 in 1d' || depletion_dice_size
           ELSE '1-' || depletion_target || ' in 1d' || depletion_dice_size
       END as depletion,
       depletion_notes,
       notes
   FROM artifact_templates
   WHERE source_name = 'CSRD Science Fiction'
   ORDER BY name

   {% for row in cursor %}
   {{row.name}}
   {{row.name|length * "-"}}

   Level:
       {{row.level}}

   Form:
       {{row.form|indent}}

   Effect:
       {{row.effect|html2rst|indent}}

   Depletion:
       {{row.depletion}} {{row.depletion_notes}}

   {{row.notes|html2rst}}
   {% endfor %}

Starships
=========

Here are a few sample starship types:

=========== ================= ===== ==============
Starship    Level             Crew  Weapon Systems
=========== ================= ===== ==============
Fighter     1                 1     1
Interceptor 2                 1     1
Freighter   3 (4 for defense) 4     1
Frigate     4                 20    4
Cruiser     4                 25    5
Battleship  10                1,000 36
=========== ================= ===== ==============

“Crew” indicates the minimum number of people needed to operate the
ship. Many ships can carry more passengers. “Weapon Systems” indicates
the maximum number of different enemies the ship can target at once—but
only one attack per target in any circumstance.

(Since it’s frighteningly easy to die in a space battle if your ship is
destroyed, most ships have escape pods. Even fighter craft have ejection
systems that put the pilot out into space in an environment suit. In
other words, GMs should try to give PCs a way out of immediately dying
if they get on the wrong end of a space battle.)

Effects of Gravity
==================

In a hard science fiction game, variable effects of gravity can’t be
waved away by tech that simulates normal gravity on spacecraft, space
stations, and other worlds. Instead, it’s an issue people must overcome.

Short-Term Microgravity Exposure:
    People new to low gravity might get
    space sickness. Newcomers must succeed on a difficulty 3 Might task or
    suffer mild nausea for about two to four days, during which time all
    their tasks are hindered. A few unlucky travelers (usually those who
    roll a 1 or otherwise face a GM intrusion) are almost completely
    incapacitated, and find all tasks hindered by three steps.

Long-Term Microgravity Exposure:
    Long-term exposure to microgravity
    environments without medical interventions degrades health. How long one
    spends in such conditions is directly relevant. The GM may assign
    long-term penalties to PCs if the situation warrants it, though the use
    of advanced space medicine, proper exercise, and recommended steroids
    and other hormones can avoid these complications.

Low Gravity:
    Weapons that rely on weight, such as all heavy weapons,
    inflict 2 fewer points of damage (dealing a minimum of 1 point).
    Short-range weapons can reach to long range, and long-range weapons can
    reach to very long range. Characters trained in low-gravity maneuvering
    ignore the damage penalty.

High Gravity:
    It’s hard to make effective attacks when the pull of
    gravity is very strong. Attacks (and all physical actions) made in
    high gravity are hindered. Ranges in high gravity are reduced by one
    category (very-long-range weapons reach only to long range, long-range
    weapons reach only to short range, and
    short-range weapons reach only to immediate range). Characters trained
    in high-gravity maneuvering ignore the change in difficulty but not
    the range decreases.

Zero Gravity:
    It’s hard to maneuver in an environment without gravity.
    Attacks (and all physical actions) made in zero gravity are hindered.
    Short-range weapons can reach to long range, and long-range weapons can
    reach to very-long range.

Effects of Vacuum
=================

Vacuum is lethal. There’s no air to breathe, and the lack of pressure
causes havoc on an organic body. An unprotected character moves one step
down the damage track each round. However, at the point where they
should die, they instead fall unconscious and remain so for about a
minute. If they are rescued during that time, they can be revived. If
not, they die.

Traveling the Solar System and Orbital Mechanics
================================================

In a hard science fiction setting, you might be interested in evoking
the reality of travel times between colonies on planets and moons in the
solar system. Even so, plotting a course between locations in the solar
system isn’t simple, because everything is always moving with respect to
everything else. You could determine exactly how long a trip would take
with some internet research. Or you could just evoke the effect of
orbital mechanics and varying accelerations on interplanetary travel.
Use the Interplanetary Travel Table to do so. For a trip between
locations not directly compared, add up the destinations in between. The
travel times assume a nuclear plasma engine of a kind already being
tested today (but better), a steady thrust toward the destination, and
an equally long and steady braking thrust over the last half of the trip
before orbit insertion. Such propulsion systems can change velocity and
sustain thrust for days at a time, which reduces bone loss, muscle
atrophy, and other long-term effects of low gravity.

Regardless, the travel times between distant locations bring home one
thing: space is big and lonely.

Interplanetary Travel

============= ===================== =====================
Origin        Destination           Travel Time Using
                                    Nuclear Plasma Engine
============= ===================== =====================
Earth/Moon    Mars                  20 + 1d20 days
Mars          Asteroid belt         30 + 1d20 days
Asteroid belt Jupiter and its moons 30 + 1d20 days
Jupiter       Saturn and its moons  60 + 1d20 days
Saturn        Uranus                90 + 1d20 days
============= ===================== =====================

Science Fiction Species Descriptors
===================================

In a science fiction setting, some GMs may want to offer alien species
or androids, who are mechanically different from humans, as options for
player characters. This can be accomplished by using descriptors. Two
examples are below.

Artificially Intelligent
------------------------

You are a machine—not just a sentient machine, but a sapient one. Your
awareness might make you an exception, or there may be many like you,
depending on the setting.

Artificially intelligent characters have machine minds of one type or
another. This can involve an advanced computer brain, but it could also
be a liquid computer, a quantum computer, or a network of smart dust
particles creating an ambient intelligence. You might even have been an
organic creature whose mind was uploaded into a machine.

Your body, of course, is also a machine. Most people refer to you as a
robot or an android, although you know neither term describes you very
well, as you are as free-willed and free-thinking
as they are.

You gain the following characteristics:

Superintelligent:
    +4 to your Intellect Pool.

Artificial Body:
    +3 to your Might Pool and your Speed Pool.

Shell:
    +1 to Armor.

Limited Recovery:
    Resting restores points only to your Intellect Pool,
    not to your Might Pool or your Speed Pool.

Mechanics, Not Medicines:
    Conventional healing methods, including the
    vast majority of restorative devices and medicines, do not restore
    points to any of your Pools. You can recover points to your Intellect
    Pool only by resting, and you can recover points to your Speed and Might
    Pools only through repair. The difficulty of the repair task is equal to
    the number of points of damage sustained, to a maximum of 10. Repairing
    your Might and Speed Pools are always two different tasks.

Machine Vulnerabilities and Invulnerabilities:
    Damaging effects and
    other threats that rely on an organic system—poison, disease, cell
    disruption, and so on—have no effect on you. Neither do beneficial drugs
    or other effects. Conversely, things that normally affect only inorganic
    or inanimate objects can affect you, as can effects that disrupt
    machines.

Uncanny Valley:
    You have a hard time relating to organic beings, and
    they don’t react well to you. All positive interaction tasks with such
    beings are hindered by two steps.

Quintar
-------

You are a quintar from the planet Quint. You are basically humanoid but
taller, thinner, and blue skinned. Your hands end in three very long
fingers. Quintar have five genders, but all quintar prefer to be
addressed as female when communicating with more binary species. Human
emotions and sexuality fascinate them, but not because they don’t have
such concepts—quintar emotions and sexuality are just very different
from those of humans. In general, quintar are more cerebral than other
species, valuing knowledge over all else.

Quint is relatively Earthlike, with slightly less gravity but a slightly
denser atmosphere.

You gain the following characteristics:

Cerebral:
    +4 to your Intellect Pool.

Skill:
    You are trained in one type of knowledge task of your choice.

Skill:
    Quintar fascination with human behavior eases all interaction
    rolls (pleasant or not) with humans.

Difficult Rest:
    Quintar subtract 2 from all recovery rolls (minimum 1).

