from dataclasses import dataclass
from docutils import nodes
from glob import glob
from pathlib import Path
from sphinx.util.docutils import SphinxDirective
from sphinx.ext.graphviz import graphviz
import sqlite3
import sys
from typing import Any, Iterator

from database.build import build_database
from extensions import common


def iter_tables(conn: sqlite3.Connection) -> Iterator[str]:
    curs = conn.cursor()
    curs.execute("PRAGMA main.table_list;")
    for result in curs:
        if result["name"].startswith("sqlite_"):
            # This is SQLite metadata, not our data.
            continue

        if result["type"] != "table":
            # This is not actual data we're storing
            continue

        yield result["name"]


@dataclass
class TableColumn:
    name: str
    primary_key_part: bool


def iter_columns(conn: sqlite3.Connection, table: str) -> Iterator[TableColumn]:
    curs = conn.cursor()
    # SQLite doesn't allow parameterised queries with PRAGMAs.
    curs.execute("PRAGMA main.table_info({});".format(table))
    for result in curs:
        yield TableColumn(result["name"], result["pk"] > 0)


@dataclass
class ColumnReference:
    from_field: str
    to_table: str
    to_field: str


def iter_foreign_keys(
    conn: sqlite3.Connection, table: str
) -> Iterator[ColumnReference]:
    curs = conn.cursor()
    # SQLite doesn't allow parameterised queries with PRAGMAs.
    curs.execute("PRAGMA foreign_key_list({});".format(table))
    for result in curs:
        yield ColumnReference(result["from"], result["table"], result["to"])


def format_graphviz_table_column(column: TableColumn) -> str:
    key_flag = "🔑" if column.primary_key_part else "&nbsp;"

    return f"""
        <TR>
            <TD PORT="{column.name}_to">{key_flag}</TD>
            <TD PORT="{column.name}_from" WIDTH="16.0">{column.name}</TD>
        </TR>
        """


def format_graphviz_table(name: str, columns: Iterator[TableColumn]) -> str:
    parts = [
        '<TABLE BORDER="0" CELLSPACING="0" CELLBORDER="1">\n',
        '<TR><TD COLSPAN="2"><B>',
        name,
        "</B></TD></TR>\n",
    ]

    for each in columns:
        parts.append(format_graphviz_table_column(each))

    parts.append("</TABLE>")

    return "".join(parts)


def format_graphviz_graph(
    tables: dict[str, tuple[Iterator[TableColumn], Iterator[ColumnReference]]]
) -> str:
    parts = [
        "digraph structs {\n",
        "node [shape=none, margin=0]\n",
    ]

    for table, (columns, references) in tables.items():
        parts.append(f"{table} [label=<")
        parts.append(format_graphviz_table(table, columns))
        parts.append(">];\n")

        for each in references:
            parts.append(f"{table}:{each.from_field}_from:e -> ")
            parts.append(f"{each.to_table}:{each.to_field}_to:w\n")

    parts.append("}")

    return "".join(parts)


def collect_schema_structure(
    conn: sqlite3.Connection,
) -> dict[str, tuple[Iterator[TableColumn], Iterator[ColumnReference]]]:
    res = {}

    for table in iter_tables(conn):
        res[table] = (
            iter_columns(conn, table),
            iter_foreign_keys(conn, table),
        )

    return res


def main(args: list[str]) -> int:
    conn = sqlite3.connect(":memory:")
    conn.row_factory = sqlite3.Row
    curs = conn.cursor()
    with open(args[1], "r") as schema_handle:
        schema_data = schema_handle.read()
    curs.executescript(schema_data)

    tables = collect_schema_structure(conn)
    print(format_graphviz_graph(tables))

    return 0


class SqlViz(SphinxDirective):
    def run(self) -> list[nodes.Node]:
        self.env.note_dependency(__file__)

        conn, data_files = common.ensure_database()
        for each in data_files:
            self.env.note_dependency(each)

        tables = collect_schema_structure(conn)

        node = graphviz()
        node["code"] = format_graphviz_graph(tables)
        node["options"] = {"docname": self.env.docname}
        return [node]


def setup(app: "sphinx.application.Sphinx") -> dict[str, Any]:
    app.setup_extension("sphinx.ext.graphviz")
    app.add_directive("sqlviz", SqlViz)

    return {
        "version": "1.0",
        "parallel_read_safe": True,
    }


if __name__ == "__main__":
    sys.exit(main(sys.argv))
