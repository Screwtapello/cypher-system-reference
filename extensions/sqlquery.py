from docutils import nodes
from docutils import statemachine
from html.parser import HTMLParser
from io import StringIO
import jinja2
from pathlib import Path
from sphinx.util.nodes import nested_parse_with_titles
from sphinx.util.docutils import SphinxDirective, switch_source_input
import sqlite3
import sys
from typing import Any, Optional, Iterable

from extensions import common


def iter_indented_lines(
    lines: Iterable[str], indent: str, first_line: str
) -> Iterable[str]:
    iter_lines = iter(lines)
    yield first_line + next(iter_lines)
    for each in iter_lines:
        yield indent + each


def indent_string(
    text: str, indent: str = "    ", first_line: Optional[str] = None
) -> str:
    if first_line is None:
        first_line = indent

    if not text:
        return indent

    lines = text.splitlines()
    return "\n".join(iter_indented_lines(text.splitlines(), indent, first_line))


class HTMLReSTConverter(HTMLParser):
    def __init__(self, output: StringIO) -> None:
        super().__init__()
        self.output = output
        self.buffers = [""]
        self.table_has_header = False
        self.div_classes: list[Optional[str]] = []

    def close(self) -> None:
        super().close()

        for each in self.buffers:
            self.output.write(each)

    def handle_starttag(
        self, tag: str, attrs: list[tuple[str, Optional[str]]]
    ) -> None:
        self.buffers.append("")

        if tag == "table":
            self.table_has_header = False
        elif tag == "thead":
            self.table_has_header = True
        elif tag == "div":
            attrs_dict = dict(attrs)
            self.div_classes.append(attrs_dict.get("class"))

    def handle_endtag(self, tag: str) -> None:
        content = self.buffers.pop()

        if tag == "p":
            self.handle_data(content.strip() + "\n\n")
            self.handle_data("\n\n")
        elif tag == "em":
            self.handle_data(f"\\ *{content}*\\ ")
        elif tag == "strong":
            self.handle_data(f"\\ **{content}**\\ ")
        elif tag in {"td", "th", "li"}:
            self.handle_data(indent_string(content.strip(), "  ", "- ") + "\n")
        elif tag == "tr":
            self.handle_data(indent_string(content.strip(), "  ", "* ") + "\n")
        elif tag == "table":
            self.handle_data(".. list-table::\n")
            self.handle_data("   :widths: auto\n")
            if self.table_has_header:
                self.handle_data("   :header-rows: 1\n")
            self.handle_data(
                "\n" + indent_string(content.strip(), "   ") + "\n\n"
            )
        elif tag == "ul":
            self.handle_data(content + "\n")
        elif tag in {"thead", "tbody"}:
            # Ignorable
            self.handle_data(content.strip() + "\n")
        elif tag == "div":
            div_class = self.div_classes.pop()
            if div_class == "admonition note":
                self.handle_data(".. note::\n")
                self.handle_data(indent_string(content.strip(), "   ") + "\n")
            else:
                print("Unhandled div class", div_class, file=sys.stderr)
                self.handle_data(content.strip() + "\n\n")
        else:
            print(f"Ignoring tag {tag}", file=sys.stderr)

    def handle_data(self, data: str) -> None:
        if not data.isspace():
            self.buffers[-1] += data


def html2rst(text: str) -> str:
    res = StringIO()
    parser = HTMLReSTConverter(res)
    parser.feed(text)
    parser.close()
    return res.getvalue()


class SqlQuery(SphinxDirective):
    has_content = True

    def run(self) -> list[nodes.Node]:
        self.env.note_dependency(__file__)

        conn, data_files = common.ensure_database()
        for each in data_files:
            self.env.note_dependency(each)

        # Split the content into a query and a template
        for (index, each) in enumerate(self.content):
            if each.strip() == "":
                query = "\n".join(self.content[:index])
                template_source = "\n".join(self.content[index + 1 :])
                break
        else:
            raise self.error(
                "sqlquery directive requires "
                "blank line between query and markup"
            )

        # Execute the query
        curs = conn.cursor()
        curs.execute(query)

        # Expand the template
        env = jinja2.Environment(
            autoescape=False, trim_blocks=True, lstrip_blocks=True
        )
        env.filters["html2rst"] = html2rst
        template = env.from_string(template_source)
        rendered = template.render(cursor=curs)

        new_source = statemachine.StringList(
            rendered.splitlines(), source="<sqlquery-output>"
        )
        with switch_source_input(self.state, new_source):
            container = nodes.container()
            nested_parse_with_titles(
                self.state,
                new_source,
                container,
            )

        return container.children  # type: ignore


def setup(app: "sphinx.application.Sphinx") -> dict[str, Any]:
    app.add_directive("sqlquery", SqlQuery)

    return {
        "version": "1.0",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
