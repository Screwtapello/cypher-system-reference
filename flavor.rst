Flavor
######

Flavors are groups of special abilities the GM and players can use to
alter a character type to make it more to their liking or more
appropriate to the genre or setting. For example, if a player wants to
create a magic-using thief character, she could play an :ref:`type:Adept` with
stealth flavoring. In a science fiction setting, a :ref:`type:Warrior` might also
have knowledge of machinery, so the character could be flavored with
technology.

At a given tier, abilities from a flavor are traded one for one with
standard abilities from a type. So to add the :ref:`Danger Sense <Danger Sense>` stealth
flavor ability to a Warrior, something else—perhaps :ref:`Bash <Bash>`—must be
sacrificed. Now that character can choose Danger Sense as they would any
other first-tier warrior ability, but they can never choose Bash.

The GM should always be involved in flavoring a type. For example, they
might know that for their science fiction game, they want a type called
a “Glam,” which is a :ref:`type:Speaker` flavored with certain technology
abilities—specifically those that make the character a flamboyant
starship pilot. Thus, they exchange the first-tier abilities
:ref:`Spin Identity <Spin Identity>`
and :ref:`Inspire Aggression <Inspire Aggression>`
for the technology flavor abilities
:ref:`Datajack <Datajack>` and :ref:`Tech Skills <Tech Skills>`
so the character can plug into the ship
directly and can take piloting and computers as skills.

In the end, flavor is mostly a tool for the GM to easily create
campaign-specific types by making a few slight alterations to the four
base types. Although players may wish to use flavors to get the
characters they want, remember that they can also shape their PCs with
descriptors and foci very nicely.

The flavors available are
:ref:`Stealth <flavor:Stealth Flavor>`,
:ref:`Technology <flavor:Technology Flavor>`,
:ref:`Magic <flavor:Magic Flavor>`,
:ref:`Combat <flavor:Combat Flavor>`,
and :ref:`Skills and Knowledge <flavor:Skills and Knowledge Flavor>`.

The full description for each listed ability can be found in the
:doc:`abilities` chapter, which also contains descriptions for type and focus
abilities in a single vast catalog.

Stealth Flavor
==============

Characters with the stealth flavor are good at sneaking around,
infiltrating places they don’t belong, and deceiving others. They use
these abilities in a variety of ways, including combat. An Explorer with
stealth flavor might be a thief, while a Warrior with stealth flavor
might be an assassin. An Explorer with stealth flavor in a superhero
setting might be a crimefighter who stalks the streets at night.

First-Tier Stealth Abilities
----------------------------

* :ref:`Danger Sense <Danger Sense>`
* :ref:`Goad <Goad>`
* :ref:`Legerdemain <Legerdemain>`
* :ref:`Opportunist <Opportunist>`
* :ref:`Stealth Skills <Stealth Skills>`

Second-Tier Stealth Abilities
-----------------------------

* :ref:`Contortionist <Contortionist>`
* :ref:`Find an Opening <Find an Opening>`
* :ref:`Get Away <Get Away>`
* :ref:`Sense Ambush <Sense Ambush>`
* :ref:`Surprise Attack <Surprise Attack>`

Third-Tier Stealth Abilities
----------------------------

* :ref:`Evanesce <Evanesce>`
* :ref:`From the Shadows <From the Shadows>`
* :ref:`Gambler <Gambler>`
* :ref:`Inner Defense <Inner Defense>`
* :ref:`Misdirect <Misdirect>`
* :ref:`Run and Fight <Run and Fight>`
* :ref:`Seize the Moment <Seize the Moment>`

Fourth-Tier Stealth Abilities
-----------------------------

* :ref:`Ambusher <Ambusher>`
* :ref:`Debilitating Strike <Debilitating Strike>`
* :ref:`Outwit <Outwit>`
* :ref:`Preternatural Senses <Preternatural Senses>`
* :ref:`Tumbling Moves <Tumbling Moves>`

Fifth-Tier Stealth Abilities
----------------------------

* :ref:`Assassin Strike <Assassin Strike>`
* :ref:`Mask <Mask>`
* :ref:`Return to Sender <Return to Sender>`
* :ref:`Uncanny Luck <Uncanny Luck>`

Sixth-Tier Stealth Abilities
----------------------------

* :ref:`Exploit Advantage <Exploit Advantage>`
* :ref:`Spring Away <Spring Away>`
* :ref:`Thief’s Luck <Thief’s Luck>`
* :ref:`Twist of Fate <Twist of Fate>`

Technology Flavor
=================

Characters with a flavor of technology typically are from science
fiction or at least modern-day
settings (although anything is possible). They excel at using, dealing
with, and building machines. An Explorer with technology flavor might
be a starship pilot, and a Speaker flavored with technology could be a
techno-priest.

Some of the less computer-oriented abilities might be appropriate for a
steampunk character, while a modern-day character could use some of the
abilities that don’t involve starships or ultratech.

First-Tier Technology Abilities
-------------------------------

* :ref:`Datajack <Datajack>`
* :ref:`Hacker <Hacker>`
* :ref:`Machine Interface <Machine Interface>`
* :ref:`Scramble Machine <Scramble Machine>`
* :ref:`Tech Skills <Tech Skills>`
* :ref:`Tinker <Tinker>`

Second-Tier Technology Abilities
--------------------------------

* :ref:`Distant Interface <Distant Interface>`
* :ref:`Machine Efficiency <Machine Efficiency>`
* :ref:`Overload Machine <Overload Machine>`
* :ref:`Serv-0 <Serv-0>`
* :ref:`Serv-0 Defender <Serv-0 Defender>`
* :ref:`Serv-0 Repair <Serv-0 Repair>`
* :ref:`Tool Mastery <Tool Mastery>`

Third-Tier Technology Abilities
-------------------------------

* :ref:`Mechanical Telepathy <Mechanical Telepathy>`
* :ref:`Serv-0 Scanner <Serv-0 Scanner>`
* :ref:`Ship Footing <Ship Footing>`
* :ref:`Shipspeak <Shipspeak>`
* :ref:`Spray <Spray>`

Fourth-Tier Technology Abilities
--------------------------------

* :ref:`Machine Bond <Machine Bond>`
* :ref:`Robot Fighter <Robot Fighter>`
* :ref:`Serv-0 Aim <Serv-0 Aim>`
* :ref:`Serv-0 Brawler <Serv-0 Brawler>`
* :ref:`Serv-0 Spy <Serv-0 Spy>`

Fifth-Tier Technology Abilities
-------------------------------

* :ref:`Control Machine <Control Machine>`
* :ref:`Jury-Rig <Jury-Rig>`
* :ref:`Machine Companion <Machine Companion>`

Sixth-Tier Technology Abilities
-------------------------------

* :ref:`Information Gathering <Information Gathering>`
* :ref:`Master Machine <Master Machine>`

Magic Flavor
============

You know a little about magic. You might not be a wizard, but you know
the basics—how it works, and how to accomplish a few wondrous things. Of
course, in your setting, “magic” might actually mean psychic powers,
mutant abilities, weird alien tech, or anything else that produces
interesting and useful effects.

An Explorer flavored with magic might be a wizard-hunter, and a Speaker
with magical flavor might be a sorcerer-bard. Although an Adept flavored
with magic is still an Adept, you might find that swapping some of the
type’s basic abilities with those given here tailors the character in
desirable ways.

First-Tier Magic Abilities
--------------------------

* :ref:`Blessing of the Gods <Blessing of the Gods>`
* :ref:`Closed Mind <Closed Mind>`
* :ref:`Entangling Force <Entangling Force>`
* :ref:`Hedge Magic <Hedge Magic>`
* :ref:`Magic Training <Magic Training>`
* :ref:`Mental Link <Mental Link>`
* :ref:`Premonition <Premonition>`

Second-Tier Magic Abilities
---------------------------

* :ref:`Concussive Blast <Concussive Blast>`
* :ref:`Fetch <Fetch>`
* :ref:`Force Field <Force Field>`
* :ref:`Lock <Lock>`
* :ref:`Repair Flesh <Repair Flesh>`

Third-Tier Magic Abilities
--------------------------

* :ref:`Distance Viewing <Distance Viewing>`
* :ref:`Fire Bloom <Fire Bloom>`
* :ref:`Fling <Fling>`
* :ref:`Force at Distance <Force at Distance>`
* :ref:`Summon Giant Spider <Summon Giant Spider>`

Fourth-Tier Magic Abilities
---------------------------

* :ref:`Elemental Protection <Elemental Protection>`
* :ref:`Ignition <Ignition>`
* :ref:`Pry Open <Pry Open>`

Fifth-Tier Magic Abilities
--------------------------

* :ref:`Create <Create>`
* :ref:`Divine Intervention <Divine Intervention>`
* :ref:`Dragon’s Maw <Dragon’s Maw>`
* :ref:`Fast Travel <Fast Travel>`
* :ref:`True Senses <True Senses>`

Sixth-Tier Magic Abilities
--------------------------

* :ref:`Relocate <Relocate>`
* :ref:`Summon Demon <Summon Demon>`
* :ref:`Traverse the Worlds <Traverse the Worlds>`
* :ref:`Word of Death <Word of Death>`

Combat Flavor
=============

Combat flavor makes a character more martial. A Speaker with combat
flavor in a fantasy setting would be a battle bard. An Explorer with
combat flavor in a historical game might be a pirate. An Adept flavored
with combat in a science fiction setting could be a veteran of a
thousand psychic wars.

First-Tier Combat Abilities
---------------------------

* :ref:`Danger Sense <Danger Sense>`
* :ref:`Practiced in Armor <Practiced in Armor>`
* :ref:`Practiced With Medium Weapons <Practiced With Medium Weapons>`

Second-Tier Combat Abilities
----------------------------

* :ref:`Bloodlust <Bloodlust>`
* :ref:`Combat Prowess <Combat Prowess>`
* :ref:`Trained Without Armor <Trained Without Armor>`

Third-Tier Combat Abilities
---------------------------

* :ref:`Practiced With All Weapons <Practiced With All Weapons>`
* :ref:`Skill With Attacks <Skill With Attacks>`
* :ref:`Skill With Defense <Skill With Defense>`
* :ref:`Successive Attack <Successive Attack>`

Fourth-Tier Combat Abilities
----------------------------

* :ref:`Capable Warrior <Capable Warrior>`
* :ref:`Deadly Aim <Deadly Aim>`
* :ref:`Fury <Fury>`
* :ref:`Misdirect <Misdirect>`
* :ref:`Spray <Spray>`

Fifth-Tier Combat Abilities
---------------------------

* :ref:`Experienced Defender <Experienced Defender>`
* :ref:`Hard Target <Hard Target>`
* :ref:`Parry <Parry>`

Sixth-Tier Combat Abilities
---------------------------

* :ref:`Greater Skill With Attacks <Greater Skill With Attacks>`
* :ref:`Mastery in Armor <Mastery in Armor>`
* :ref:`Mastery With Defense <Mastery With Defense>`

Skills and Knowledge Flavor
===========================

This flavor is for characters in roles that call for more knowledge
and more real-world application of talent. It’s less flashy and
dramatic than supernatural powers or the ability to hack apart
multiple foes, but sometimes expertise or
know-how is the real solution to a problem.

A Warrior flavored with skills and knowledge might be a military
engineer. An Explorer flavored with skills and knowledge could be a
field scientist. A Speaker with this flavor might be a teacher.

First-Tier Skills and Knowledge Abilities
-----------------------------------------

* :ref:`Interaction Skills <Interaction Skills>`
* :ref:`Investigative Skills <Investigative Skills>`
* :ref:`Knowledge Skills <Knowledge Skills>`
* :ref:`Physical Skills <Physical Skills>`
* :ref:`Travel Skills <Travel Skills>`

Second-Tier Skills and Knowledge Abilities
------------------------------------------

* :ref:`Extra Skill <Extra Skill>`
* :ref:`Tool Mastery <Tool Mastery>`
* :ref:`Understanding <Understanding>`

Third-Tier Skills and Knowledge Abilities
-----------------------------------------

* :ref:`Flex Skill <Flex Skill>`
* :ref:`Improvise <Improvise>`

Fourth-Tier Skills and Knowledge Abilities
------------------------------------------

* :ref:`Multiple Skills <Multiple Skills>`
* :ref:`Quick Wits <Quick Wits>`
* :ref:`Task Specialization <Task Specialization>`

Fifth-Tier Skills and Knowledge Abilities
-----------------------------------------

* :ref:`Practiced With Medium Weapons <Practiced With Medium Weapons>`
* :ref:`Read the Signs <Read the Signs>`

Sixth-Tier Skills and Knowledge Abilities
-----------------------------------------

* :ref:`Skill With Attacks <Skill With Attacks>`
* :ref:`Skill With Defense <Skill With Defense>`
